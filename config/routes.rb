Rails.application.routes.draw do
  resources :exports

  post "create_file" =>"exports#create_file"

  root "exports#index"
end
