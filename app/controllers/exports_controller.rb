class ExportsController < ApplicationController
  require 'csv'
  before_action :set_export, only: [:show, :edit, :update, :destroy]

  # GET /exports
  # GET /exports.json
  def index
    @exports = Export.all
  end

  # GET /exports/1
  # GET /exports/1.json
  def show

    @export.file_csv.open do |file|
      @csv_content = CSV.read(file, headers: true).take(5)
    end
    @headers = @csv_content.first.headers
  end

  # GET /exports/new
  def new
    @export = Export.new
  end

  # GET /exports/1/edit
  def edit
  end

  # POST /exports
  # POST /exports.json
  def create
    @export = Export.new(export_params)

    respond_to do |format|
      if @export.save
        format.html { redirect_to @export, notice: 'Export was successfully created.' }
        format.json { render :show, status: :created, location: @export }
      else
        format.html { render :new }
        format.json { render json: @export.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /exports/1
  # PATCH/PUT /exports/1.json
  def update
    respond_to do |format|
      if @export.update(export_params)
        format.html { redirect_to @export, notice: 'Export was successfully updated.' }
        format.json { render :show, status: :ok, location: @export }
      else
        format.html { render :edit }
        format.json { render json: @export.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /exports/1
  # DELETE /exports/1.json
  def destroy
    @export.destroy
    respond_to do |format|
      format.html { redirect_to exports_url, notice: 'Export was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  # Creates a CSV file and stores it in active storage (S3)
  def create_file

    # Create a instance of Export
    @export = Export.new(model: "test programatic", name: "from app" )

    # Reference local folder to temporarily store the file and name
    file_path = "public/csv/"
    filename = "CSV_#{Time.now.strftime("%d_%b_%Y_%H%M")}.csv"

    # Create a csv file, rewind and close it
    csv_file = CSV.open("#{file_path}#{filename}", 'w')
    csv_file.rewind # Can add content here, such as headers
    csv_file.close

    # Attach it to the @export record
    @export.file_csv.attach(io: File.open("#{file_path}#{filename}"), filename: filename, content_type: "text/csv")

    # Save both
    @export.save

    # Redirect to root
    redirect_to root_path

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_export
      if params[:id] == "create_file"
        @export = Export.find(1)
      else
        @export = Export.find(params[:id])
      end
    end

    # Only allow a list of trusted parameters through.
    def export_params
      params.require(:export).permit(:model, :name, :file_csv)
    end
end

