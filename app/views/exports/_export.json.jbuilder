json.extract! export, :id, :model, :name, :created_at, :updated_at
json.url export_url(export, format: :json)
